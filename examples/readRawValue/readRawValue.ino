/**
    @author: AMOUSSOU Z. Kenneth
    @date: 15-01-2019
    @summary: Example program to retrieve raw data from MPU9250 sensor
    @chip: tested on MPU9250
    @platform: Arduino
*/
#include <MPU9250.h>

MPU9250 imu;

IMU_DATA data;

void setup() {  
  Serial.begin(9600);
  Serial.println("Init + Calibration \n");
  
  imu.begin();
  
  /*
    Read the device I2C adress
    Should be:
        0x68    : when AD0 is connceted to GND
        0x69    : when AD0 is connected to VCC

    # Check your connection if you get different value. #
  */
  /* Check correct wiring of the sensor */
  while(imu.whoami() != ID){
    Serial.println("MPU9250 not found!");
    delay(200);     // 200ms
  }
  Serial.print("Sensor ADDR: 0x");
  Serial.println(imu.whoami(), HEX);
}

void loop() {
  /**
    Read raw data from the IMU sensor
    
    These data are quite meaningless.
    
    data:
        struct{
            float X;
            float Y;
            float Z;
        }XYZ_DATA
        
        struct{
            XYZ_DATA accel;
            XYZ_DATA gyro;
            XYZ_DATA mag;
            float temp;
        }IMU_DATA
        
    Use this data structure to retrieve the information needed
  */
  data = imu.getData();
  data.mag = imu.magReadRawData();

  Serial.print("Gyroscope x, y, z: ");
  Serial.print(data.gyro.X);
  Serial.print(", ");
  Serial.print(data.gyro.Y);
  Serial.print(", ");
  Serial.println(data.gyro.Z);

  Serial.print("Accelerometer x, y, z: ");
  Serial.print(data.accel.X);
  Serial.print(", ");
  Serial.print(data.accel.Y);
  Serial.print(", ");
  Serial.println(data.accel.Z);
  
  Serial.print("Magnetometer x, y, z: ");
  Serial.print(data.mag.X);
  Serial.print(", ");
  Serial.print(data.mag.Y);
  Serial.print(", ");
  Serial.println(data.mag.Z);

  Serial.print("Temperature: ");
  Serial.println(data.temp);

  Serial.println();
  
  delay(1000);

}
