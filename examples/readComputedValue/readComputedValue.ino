/**
    @author: AMOUSSOU Z. Kenneth
    @date: 14-01-2019
    @summary: Example program to retrieve computed data from MPU9250 sensor
    @chip: tested on MPU9250
    @platform: Arduino
*/
#include <MPU9250.h>

MPU9250 imu;

IMU_DATA data;

void setup() {
  /* Initialize serial communication */
  Serial.begin(9600);
  Serial.println("Initialization + calibration \n");
  
  /* Initialize sensor for measurement 
     Disable sleep mode + set up full scale range
  */
  imu.begin();
  
  /* Check correct wiring of the sensor */
  while(imu.whoami() != ID){
    Serial.println("MPU60x0 not found!");
    delay(200);     // 200ms
  }
  /* Read device I2C adress */
  Serial.print("Sensor ADDR: 0x");
  Serial.println(imu.whoami(), HEX);
  /* Read Magnetometer device I2C adress */
  Serial.print("Magnetometer ADDR: 0x");
  Serial.println(imu.mag_whoami(), HEX);
  
  // Initialization
  /*
  Serial.println("Start calibation ...");
  imu.calibrate();
  Serial.println("End of calibation\n");
  */
}

void loop() {
  /**
    Read the IMU sensor computed data
    
    Gyroscope:      °/s (degree per second)
    Accelerometer:  g
    Magnetometer T (Tesla)
    Temperature:    °C 
  */
  data = imu.read();

  Serial.print("Gyroscope x, y, z: ");
  Serial.print(data.gyro.X);
  Serial.print(" °/s , ");
  Serial.print(data.gyro.Y);
  Serial.print(" °/s , ");
  Serial.print(data.gyro.Z);
  Serial.println(" °/s");

  Serial.print("Accelerometer x, y, z: ");
  Serial.print(data.accel.X);
  Serial.print(" g , ");
  Serial.print(data.accel.Y);
  Serial.print(" g , ");
  Serial.print(data.accel.Z);
  Serial.println(" g");
  
  Serial.print("Magnetometer x, y, z: ");
  Serial.print(data.mag.X);
  Serial.print(" µT , ");
  Serial.print(data.mag.Y);
  Serial.print(" µT , ");
  Serial.print(data.mag.Z);
  Serial.println(" µT");

  Serial.print("Temperature: ");
  Serial.print(data.temp);
  Serial.println(" °C");

  Serial.println();
  delay(1000);
}
